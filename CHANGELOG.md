# Changelog 


## [1.6.1] - TBD
- bump dependencies
- prune redundant address field artifact
- reorder Centre fields in admin view
- add history tracking
- centre type status as both vocabulary and free text
- autoupdate certification status based on certification expiration date and notify maintainers


## [1.4.1] - 21/03/2023

### Changes
- Started CHANGELOG.md
- Variables for initialistion at specified migration
- In case of no backups initialise database from fixtures
- Upgraded Centre Registry to [2.3.5](https://github.com/clarin-eric/Centre-Registry/releases/tag/2.3.5)
- Upgraded docker-alpine-base image to [2.3.5](https://gitlab.com/CLARIN-ERIC/docker-alpine-base/-/tags/2.3.5) - Alpine 3.17.2
- Updated Alpine packages:
  - python3 to python3-dev 3.10.10-r0
  - git to 2.38.4-r1
  - xvfb to 21.1.7-r1
  - firefox-esr to firefox 109.0.1-r0
  - py3-lxml to 4.9.2-r0
  - py3-jsonschema to 4.7.2-r2
- Added Alpine packages:
  - tzdata 2022f-r1
- Updated Python packages:
  - pip to 23.0.1
  - selenium to 4.8.2
- Updated custom installations:
  - geckodriver to 0.32.2
- Added Alpine package (docker build stage only):
  - build-base 0.5-r3
  - libffi-dev 3.4.4-r0
- Updated CI setup to build script to [2.0.13](https://gitlab.com/CLARIN-ERIC/build-script/-/tags/2.0.13)
