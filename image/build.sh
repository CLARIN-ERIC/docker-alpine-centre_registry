#!/bin/bash
set -e

WORK_PATH='/srv'
cd -- $WORK_PATH
VENV_PATH="${WORK_PATH}/venv/runtime/bin/activate"

# Source
CENTRE_REGISTRY_VERSION=3.0.2-a9

# Deployment dependencies
PYTHON3_VERSION=3.11.11-r0
PYTHON3_DEV_VERSION=3.11.11-r0
PYTHON3_PIP_VERSION=23.3.1-r0
PYTHON3_TZDATA_VERSION=2023.3-r3
PYTHON3_WHEEL=0.42.0-r0
GIT_VERSION=2.43.6-r0
SIGIL_VERSION=0.10.1
SIGIL_SUM_ARM="af5ce4d9a3b8089afa4e703321f1d431212b3eacf400e4313a0286a627bc4e75"
SIGIL_SUM_AMD="35f7e78f0f6320225814f4fd0f59d91251c6b778338e7a5ff3c04bc2ee5c034f"
SIGIL_SUM_ARMHF="9aafc466fc2fc67d99eba43fc00572becbba8edb9b8c4d4f9efcc6768bae727b"

# Packages for API testing
PY3_LXML_VERSION=4.9.3-r1
PY3_JSONSCHEMA=4.20.0-r0

# Packages for UI testing
SELENIUM_VERSION=4.13.0
FIREFOX_VERSION=125.0.3-r0
XVFB_VERSION=21.1.12-r0
GECKODRIVER_VERSION=0.34.0
GECKODRIVER_SUM_LINUX64="79b2e77edd02c0ec890395140d7cdc04a7ff0ec64503e62a0b74f88674ef1313"
GECKODRIVER_SUM_AARCH64="af2edce2ef3ea3289dc77ef0df4e642b382b15ef8293f188ffee782a2ccfb370"


curl_cmd='curl --fail --location --proto =https --silent --show-error --tlsv1.2'

function installWheel() {
# $1 - repository name
# $2 - release version tag
	WHEEL_NETLOC="$(curl -L https://api.github.com/repos/clarin-eric/"$1"/releases/tags/"$2" | grep "browser_download_url.*.whl" | cut -d : -f 2,3 | tr -d \" | tr -d " ")"
	WHEEL_FILENAME="$(basename "${WHEEL_NETLOC}")"
	curl -L --output "/tmp/${WHEEL_FILENAME}" "${WHEEL_NETLOC}"
	python3 -m "pip" --no-cache-dir install "/tmp/${WHEEL_FILENAME}"
}

# shellcheck source=/srv/venv/runtime/bin/activate
. "${VENV_PATH}"

apk add --no-cache python3=$PYTHON3_VERSION python3-dev=$PYTHON3_DEV_VERSION py3-pip=$PYTHON3_PIP_VERSION \
    py3-lxml=$PY3_LXML_VERSION firefox=$FIREFOX_VERSION xvfb=$XVFB_VERSION git=$GIT_VERSION \
    py3-wheel=$PYTHON3_WHEEL py3-jsonschema=$PY3_JSONSCHEMA py3-tzdata=$PYTHON3_TZDATA_VERSION

python3 -m "pip" install --upgrade pip
installWheel "Centre-Registry" ${CENTRE_REGISTRY_VERSION}


ARCH4SIGIL="amd64"
ARCH4GEKODRIVER="linux64"
SIGIL_SUM="${SIGIL_SUM_AMD}"
GECKODRIVER_SUM="${GECKODRIVER_SUM_LINUX64}"
if [ "$(arch)" == "aarch64" ]; then
    ARCH4SIGIL="arm64"
    ARCH4GEKODRIVER="linux-aarch64"
    SIGIL_SUM="${SIGIL_SUM_ARM}"
    GECKODRIVER_SUM="${GECKODRIVER_SUM_AARCH64}"
elif [ "$(arch)" == "armv7l" ]; then
    ARCH4SIGIL="armhf"
    SIGIL_SUM="${SIGIL_SUM_ARMHF}"
fi

$curl_cmd --remote-name-all \
    "https://github.com/mozilla/geckodriver/releases/download/v${GECKODRIVER_VERSION}/geckodriver-v${GECKODRIVER_VERSION}-${ARCH4GEKODRIVER}.tar.gz" \
    "https://github.com/gliderlabs/sigil/releases/download/v${SIGIL_VERSION}/gliderlabs-sigil_${SIGIL_VERSION}_Linux_${ARCH4SIGIL}.tgz"
printf "%s\n" "${GECKODRIVER_SUM} *geckodriver-v${GECKODRIVER_VERSION}-${ARCH4GEKODRIVER}.tar.gz" \
    "${SIGIL_SUM} *gliderlabs-sigil_${SIGIL_VERSION}_Linux_${ARCH4SIGIL}.tgz" | \
    sha256sum -c --strict -w
tar -p -x -z -f "geckodriver-v${GECKODRIVER_VERSION}-${ARCH4GEKODRIVER}.tar.gz" -C "/usr/local/bin/" && rm -fr "geckodriver-v${GECKODRIVER_VERSION}-${ARCH4GEKODRIVER}.tar.gz"
tar -p -x -z -f "gliderlabs-sigil_${SIGIL_VERSION}_Linux_${ARCH4SIGIL}.tgz" -C "/usr/local/bin/" && rm -fr "gliderlabs-sigil_${SIGIL_VERSION}_Linux_${ARCH4SIGIL}.tgz"
ln -s "/usr/local/bin/gliderlabs-sigil-${ARCH4SIGIL}" "/usr/local/bin/sigil"

python3 -m 'pip' --no-cache-dir install selenium==$SELENIUM_VERSION

