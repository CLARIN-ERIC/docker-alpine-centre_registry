#!/bin/bash
set -e
xvfb_teardown() {
    # Tears down everything to prevent e.g. restarting containers on reboot of
    # local test host.
    status=$?
    set +e
    killall Xvfb
    set -e
    return "$status"
}
## Check whether the required data is mounted via host volumes.
## Because of Docker Engine's odd behavior to create an empty directory for
## nonexistent host volume paths, use an elaborate check whether any files \
## exist under '/src/centre_registry_project'.
if find '/srv/centre_registry_project' -maxdepth '1' -type 'f' \
    -print | grep -q . ; then
    python3 -m "django" test 'centre_registry.migrations'
    python3 -m "django" test 'centre_registry.test_api'
    Xvfb -ac :99 -screen '0' '1280x1024x16' &
    trap xvfb_teardown INT TERM EXIT
    DISPLAY=':99'
    export DISPLAY
    python3 -m "django" test 'centre_registry.test_ui'
else
    printf '%s\n' "WARNING: Skipping tests, container has not been initialized. "
fi

