#!/bin/bash
set -e
set -x

WORK_PATH="/srv"
SITE_PACKAGES_PATH="${WORK_PATH}/venv/runtime/lib/python3.11/site-packages"
SETTINGS_PATH="${SITE_PACKAGES_PATH}/centre_registry_project/settings.py"
DATA_PROVISION_PATH="/centre_registry_config.d"
PROVISION_CONFIG_PATH="${DATA_PROVISION_PATH}/Centre-Registry-config"
PROVISION_DATABASE_PATH="${DATA_PROVISION_PATH}/Centre-Registry-database"

VENV_PATH="${WORK_PATH}/venv/runtime/bin/activate"

echo "Activating runtime Venv"
# shellcheck source=/srv/venv/runtime/bin/activate
. "${VENV_PATH}"
echo "Venv activated"


cd -- "${WORK_PATH}"

if ! [ -d "${WORK_PATH}/database" ]; then
    mkdir -p "${WORK_PATH}/database"
fi 

# Override default configuration
if [ ! -f "${PROVISION_CONFIG_PATH}/settings.py.tmpl" ]; then
    echo "Centre Registry configuration template not supplied. Using source default."
else
  if [ -z "${DJANGO_SETTINGS_MODULE}" ]; then
    echo "DJANGO_SETTINGS_MODULE is NOT set"
  elif [ -z "${DJANGO_DEBUG}" ]; then
    echo "DJANGO_DEBUG is NOT set"
  elif [ -z "${DJANGO_DB_SECRET_KEY}" ]; then
    echo "DJANGO_DB_SECRET_KEY is NOT set"
  elif [ -z "${DJANGO_PIWIK_WEBSITE_ID}" ]; then
    echo "DJANGO_PIWIK_WEBSITE_ID is NOT set"
  elif [ -z "${EMAIL_HOST}" ]; then
    echo "EMAIL_HOST is NOT set"
  else
    sigil -f "${PROVISION_CONFIG_PATH}/settings.py.tmpl" \
        debug="${DJANGO_DEBUG:?}" \
        db_secret_key="${DJANGO_DB_SECRET_KEY:?}" \
        piwik_website_id="${DJANGO_PIWIK_WEBSITE_ID:?}" \
        servername="${_SERVERNAME:?}" \
        email_host="${EMAIL_HOST}" > "${SETTINGS_PATH}"
  fi
fi

# If no data provided (1) load fixtures as initial values, otherwise (0)
DB_FROM_SCRATCH=1

# Initialize database
if [ -d "${PROVISION_DATABASE_PATH}" ]; then    
    if [ "$(find ${PROVISION_DATABASE_PATH} -name \*.sqlite -type f  | wc -l)" -gt 0 ]; then
        echo -n "Trying to copy supplied database files... "
	# Check if db target location exists
        if [ "$(find ${WORK_PATH}/database/ -name database.sqlite -type f  | wc -l)" -gt 0 ]; then
            DB_FROM_SCRATCH=0
            echo "There is already a database file installed! Leaving it intact."
        else
            # Copy supplied database on fresh start
            cp "$(unset -v latest; for file in "${PROVISION_DATABASE_PATH}"/*.sqlite; do [ ! -L "$file" ] && [[ "$file" -nt "$latest" ]] &&  latest=$file; done; echo  "$latest")" "${WORK_PATH}/database/database.sqlite"
            chown 100:101 "${WORK_PATH}"/database/database.sqlite
            DB_FROM_SCRATCH=0
            echo "Done."
	fi
    fi
fi

# Migrate to target migration if needed
# Migrations should have been committed to Git repo for Centre Registry app!
if [ -n "${DJANGO_INITIAL_MIGRATION_LABEL}" ]; then
  echo "Partial migration up to ${DJANGO_INITIAL_MIGRATION_LABEL}"
  python3 -m "django" migrate admin
  python3 -m "django" migrate auth
  python3 -m "django" migrate contenttypes
  python3 -m "django" migrate sessions
  python3 -m "django" migrate centre_registry "${DJANGO_INITIAL_MIGRATION_LABEL}"
else
  python3 -m "django" migrate
fi

# If no data source been spotted (backup, persisting volume data) load fixtures
# as initial data

if [ ${DB_FROM_SCRATCH} -eq 1 ]; then
    echo "WARNING: Initializing container from scratch, loading source's test fixtures."
    python3 -m "django" loaddata "${SITE_PACKAGES_PATH}/centre_registry/fixtures/test_data.json"
fi

python3 -m "django" collectstatic --clear --no-input
